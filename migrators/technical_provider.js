import Migrator from '../lib/migrator';

class TechnicalProviderMigrator extends Migrator {
  constructor() {
    super('technical_provider', 'tb_pat', 'TechnicalProvider');
  }

  parseRecord(record) {
    super.parseRecord(record);
    record.providerType = record.provider_type;
    record.servedHouseholds = record.served_households;
    record.score = record.score || 'N';
  }
}

const migrator = new TechnicalProviderMigrator();
migrator.migrate();
