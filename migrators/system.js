import Migrator from '../lib/migrator';

class SystemMigrator extends Migrator {
  constructor() {
    super('system', 'tb_sistema', 'System');
    this.variables = {
      eia: 'wsi',
      eiaaut: 'wsiAut',
      eiainf: 'wsiInf',
      eiazpa: 'wsiPro',
      eiastr: 'wsiTre',
    };
  }

  parseRecord(record) {
    super.parseRecord(record);
    record.buildingYear = record.building_year;
    record.supplyType = record.supply_type;
    record.servedHouseholds = record.served_households;
    record.score = record.score || Migrator.calculateScore(record.wsi);
  }

  async postMigrate() {
    let query = `
      SELECT DISTINCT
        comun_seq as community_id,
        siste_seq as system_id
      FROM tb_grp_domicilio
      WHERE siste_seq IS NOT NULL
        AND siste_seq in (
          SELECT siste_seq
          FROM tb_sistema
        )
    `;
    const result = await this.biQuery(query, { type: this.sequelize.bi.QueryTypes.SELECT });
    query = `
      INSERT INTO systems_communities (community_id, system_id)
      VALUES ${result.map(r => `(${r.community_id}, ${r.system_id})`).join(',')}
    `;
    await this.dataQuery(query, { type: this.sequelize.bi.QueryTypes.INSERT });
  }
}

const migrator = new SystemMigrator();
migrator.migrate();
