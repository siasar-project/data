import Migrator from '../lib/migrator';

class CommunityMigrator extends Migrator {
  constructor() {
    super('community', 'tb_comunidade', 'Community');
    this.variables = {
      ias: 'wsp',
      nash: 'wshl',
      issa: 'wssi',
      nsa: 'wsl',
      nsh: 'shl',
      eiacom: 'wsi',
      psecom: 'sep',
      nsaacc: 'wslAcc',
      nsacon: 'wslCon',
      nsaest: 'wslSea',
      nsacal: 'wslQua',
      nshnss: 'shlSsl',
      nshhpe: 'shlPer',
      nshhho: 'shlWat',
      nshhco: 'shlCom',
      eiaaut: 'wsiAut',
      eiainf: 'wsiInf',
      eiazpa: 'wsiPro',
      eiastr: 'wsiTre',
      psegor: 'sepOrg',
      psegom: 'sepOpm',
      psegef: 'sepEco',
      psegam: 'sepEnv',
      ecs: 'ecs',
      ecseag: 'ecsEag',
      ecscag: 'ecsCag',
      ecsshe: 'ecsShe',
      ecsshs: 'ecsShs',
      fis: 'fis',
      fps: 'fps',
      fhp: 'fhp',
      fus: 'fus',
      ftb: 'ftb',
      fafl: 'fafl',
    };
  }

  parseRecord(record) {
    super.parseRecord(record);
    record.score = record.score || Migrator.calculateScore(record.wsp);
  }

  async postMigrate() {
    console.log('Updating regions');
    const query = `
      UPDATE regions
      SET score = (
        SELECT mode()
        WITHIN GROUP (ORDER BY communities.score)
        FROM communities
        WHERE ST_DWithin(communities.geom, regions.geom, 0)
      )
    `;
    await this.dataQuery(query, { type: this.sequelize.bi.QueryTypes.UPDATE });
  }
}

const migrator = new CommunityMigrator();
migrator.migrate();
