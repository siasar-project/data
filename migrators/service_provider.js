import Migrator from '../lib/migrator';

class ServiceProviderMigrator extends Migrator {
  constructor() {
    super('service_provider', 'tb_prest_servico', 'ServiceProvider');
    this.variables = {
      pse: 'sep',
      psegor: 'sepOrg',
      psegom: 'sepOpm',
      psegef: 'sepEco',
      psegam: 'sepEnv',
    };
  }

  parseRecord(record) {
    super.parseRecord(record);
    record.providerType = record.provider_type;
    record.monthBilling = record.month_billing;
    record.legalStatus = record.legal_status;
    record.servedCommunities = record.served_communities;
    record.servedHouseholds = record.served_households;
    record.womenCount = record.women_count;
    record.score = record.score || Migrator.calculateScore(record.sep);
  }
}

const migrator = new ServiceProviderMigrator();
migrator.migrate();
