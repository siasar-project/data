import express from 'express';
import Sequelize from 'sequelize';

import models from '../models';

const router = express.Router();
const Op = Sequelize.Op;

router.get('/', (req, res, next) => {
  const filterable = ['country', 'adm0', 'adm1', 'adm2', 'adm3', 'adm4', 'score'];
  const where = {};
  Object.keys(req.query).forEach((key) => {
    if (filterable.includes(key)) where[key] = req.query[key];
  });

  res.format({
    html: () => {
      Promise.all([
        models.Community.findAll({
          where: Object.assign({
            siasar_version: '2',
            score: { [Op.not]: 'N' },
            wsp: { [Op.gt]: 0 },
          }, where),
          order: [['wsp', 'DESC']],
        }),
        models.Community.findAll({
          where: Object.assign({ score: { [Op.not]: 'N' } }, where),
          group: ['score'],
          attributes: ['score', [Sequelize.fn('COUNT', 'score'), 'count']],
          order: ['score'],
        }),
        models.Community.findAll({
          where: Object.assign({ score: { [Op.not]: 'N' } }, where),
          group: ['country'],
          attributes: ['country', [Sequelize.fn('COUNT', 'country'), 'count']],
          order: ['country'],
        }),
        models.Community.findAll({
          where: Object.assign({ score: { [Op.not]: 'N' }, surveyDate: { [Op.gt]: '2010-01-01' } }, where),
          group: ['month'],
          attributes: [[Sequelize.fn('to_char', Sequelize.col('survey_date'), 'YYYY-MM'), 'month'],
            [Sequelize.fn('COUNT', 'survey_date'), 'count']],
          order: Sequelize.literal('month'),
        }),
      ]).then((result) => {
        res.render('community/index', {
          geoJSON: {
            type: 'FeatureCollection',
            features: result[0].map(feature => ({
              type: 'Feature',
              geometry: feature.geom,
              properties: {
                siasarId: feature.siasarId,
                name: feature.name,
                score: feature.score,
              },
            })),
          },
          lastResults: result[0].slice(0, 10),
          scoreChart: {
            data: result[1].map(r => r.get('count')),
            labels: result[1].map(r => r.get('score')),
          },
          countryChart: {
            data: result[2].map(r => r.get('count')),
            labels: result[2].map(r => r.get('country')),
          },
          dateChart: {
            data: result[3].reduce((a, r, i) => [...a, parseInt(r.get('count'), 10) + (a[i - 1] || 0)], []),
            labels: result[3].map(r => r.get('month')),
          },
        });
      }).catch(next);
    },
    json: () => {
      const attributes = ['siasarId', 'name', 'country', 'adm0', 'adm1', 'adm2', 'adm3', 'adm4',
        'geom', 'score', 'population', 'households', 'wsp', 'wshl', 'wssi', 'wsl', 'shl', 'wsi',
        'sep', 'wslAcc', 'wslCon', 'wslSea', 'wslQua', 'shlSsl', 'shlPer', 'shlWat', 'ecs', 'ecsEag',
        'ecsCag', 'ecsShe', 'ecsShs', 'fis', 'fps', 'fhp', 'fus', 'ftb', 'fafl'];

      models.Community.findAll({
        attributes,
        where,
        raw: true,
      }).then((result) => {
        res.json(result);
      }).catch(next);
    },
  });
});

router.get('/:id', (req, res, next) => {
  models.Community.findOne({
    where: { siasarId: req.params.id },
    include: ['systems'],
  }).then((record) => {
    res.render('community/show', {
      record,
    });
  }).catch(next);
});

module.exports = router;
