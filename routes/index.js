import express from 'express';

import models from '../models';

const router = express.Router();

router.get('/', (req, res) => {
  Promise.all([
    models.Community.count(),
    models.System.count(),
    models.ServiceProvider.count(),
    models.TechnicalProvider.count(),
  ]).then((result) => {
    res.render('index', {
      title: 'SIASAR Data',
      result,
    });
  });
});

module.exports = router;
