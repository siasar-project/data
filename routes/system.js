import express from 'express';
import Sequelize from 'sequelize';

import models from '../models';

const router = express.Router();
const Op = Sequelize.Op;

router.get('/', (req, res, next) => {
  Promise.all([
    models.System.findAll({
      where: {
        siasar_version: '2',
        score: { [Op.not]: 'N' },
        wsi: { [Op.gt]: 0 },
      },
      order: [['surveyDate', 'DESC']],
      limit: 10,
    }),
    models.System.findAll({
      where: { score: { [Op.not]: 'N' } },
      group: ['score'],
      attributes: ['score', [Sequelize.fn('COUNT', 'score'), 'count']],
      order: ['score'],
    }),
    models.System.findAll({
      where: { score: { [Op.not]: 'N' } },
      group: ['country'],
      attributes: ['country', [Sequelize.fn('COUNT', 'country'), 'count']],
      order: ['country'],
    }),
    models.System.findAll({
      where: { score: { [Op.not]: 'N' }, surveyDate: { [Op.gt]: '2010-01-01' } },
      group: ['month'],
      attributes: [[Sequelize.fn('to_char', Sequelize.col('survey_date'), 'YYYY-MM'), 'month'],
        [Sequelize.fn('COUNT', 'survey_date'), 'count']],
      order: Sequelize.literal('month'),
    }),
  ]).then((result) => {
    res.render('system/index', {
      lastResults: result[0],
      scoreChart: {
        data: result[1].map(r => r.get('count')),
        labels: result[1].map(r => r.get('score')),
      },
      countryChart: {
        data: result[2].map(r => r.get('count')),
        labels: result[2].map(r => r.get('country')),
      },
      dateChart: {
        data: result[3].reduce((a, r, i) => [...a, parseInt(r.get('count'), 10) + (a[i - 1] || 0)], []),
        labels: result[3].map(r => r.get('month')),
      },
    });
  }).catch(next);
});

router.get('/:id', (req, res, next) => {
  models.System.findOne({
    where: { siasarId: req.params.id },
    include: ['communities'],
  }).then((result) => {
    const record = result.toJSON();
    res.render('system/show', {
      record,
    });
  }).catch(next);
});

module.exports = router;
