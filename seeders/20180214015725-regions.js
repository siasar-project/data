
import geojson from './regions.json';

module.exports = {
  up: (queryInterface, Sequelize) => {
    if (process.env.NODE_ENV === 'production') {
      return queryInterface.sequelize.query(`
        INSERT INTO regions (country, adm_level, name, geom)
        SELECT country, 1, name, geom
        FROM base.adm_1;
        INSERT INTO regions (country, adm_level, name, geom)
        SELECT country, 2, name, geom
        FROM base.adm_2;
      `);
    }
    return queryInterface.bulkInsert('regions', geojson.features.map((feature) => {
      feature.geometry.crs = { type: 'name', properties: { name: 'EPSG:4326' } };
      feature.properties.geom = Sequelize.fn('ST_GeomFromGeoJSON', JSON.stringify(feature.geometry));
      return feature.properties;
    }), {});
  },

  down: queryInterface => queryInterface.bulkDelete('regions', null, {}),
};
