module.exports = {
  development: {
    pageSize: 10000,
    metadata: true,
  },
  production: {
    pageSize: 10000,
    metadata: false,
  },
};
