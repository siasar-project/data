module.exports = {
  development: {
    username: 'username',
    password: null,
    database: 'siasar_data',
    host: '127.0.0.1',
    dialect: 'postgres',
    logging: false,
    define: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  },
  production: {
    username: process.env.SIASAR_DATA_DB_USERNAME,
    password: process.env.SIASAR_DATA_DB_PASSWORD,
    database: 'siasar_data',
    host: '127.0.0.1',
    dialect: 'postgres',
    logging: false,
    define: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  },
  bi_development: {
    username: 'username',
    password: null,
    database: 'bi',
    host: '127.0.0.1',
    dialect: 'postgres',
    logging: false,
  },
  bi_production: {
    username: process.env.SIASAR_BI_DB_USERNAME,
    password: process.env.SIASAR_BI_DB_PASSWORD,
    database: 'bi',
    host: '127.0.0.1',
    dialect: 'postgres',
    logging: false,
  },
};
