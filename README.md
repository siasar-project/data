# SIASAR Data

1. Install dependencies: `npm install`
1. Edit database connections at `config/database.js`
1. Create database: `createdb siasar_data`
1. Create PostGIS extension: `psql -c "create extension postgis" siasar_data`
1. Build code: `npm run build`
1. Setup database: `npm run db:setup`
1. Migrate data: `npm run migrate:all`

## For developers
* If you modify any JS file you have to recompile: `npm run build`
* If you wanna autocompile files when save: `npm run watch`
* To compile and run one file: `npm run execute src/migrators/community.js`
* To reset database: `npm run db:reset`
* To seed tables with demo data: `npm run db:seed`
* To run one single migration: `npm run migrate:community`