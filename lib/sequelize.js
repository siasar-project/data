import Sequelize from 'sequelize';
import databaseConfig from '../config/database';

const dataConfig = databaseConfig[process.env.NODE_ENV || 'development'];
const biConfig = databaseConfig[`bi_${process.env.NODE_ENV || 'development'}`];

export default {
  data: new Sequelize(dataConfig.database, dataConfig.username, dataConfig.password, dataConfig),
  bi: new Sequelize(biConfig.database, biConfig.username, biConfig.password, biConfig),
};
