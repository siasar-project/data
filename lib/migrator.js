import fs from 'fs';
import stamp from 'console-stamp';
import moment from 'moment';

import migratorConfig from '../config/migrator';
import sequelize from '../lib/sequelize';
import models from '../models';

export default class Migrator {
  constructor(query, table, model) {
    this.config = migratorConfig[process.env.NODE_ENV || 'development'];
    this.sequelize = sequelize;

    this.query = fs.readFileSync(`./queries/${query}.sql`, { encoding: 'utf8' });
    this.query += ' LIMIT :limit OFFSET :offset';
    this.table = table;
    this.model = models[model];

    this.variables = {};

    stamp(console, {
      formatter: () => {
        const duration = moment.duration(process.uptime(), 's');
        let time;
        if (duration.asSeconds() < 60) {
          time = `  ${duration.asSeconds().toFixed(2)}s`;
        } else {
          time = `  ${duration.asMinutes().toFixed(2)}m`;
        }
        return `${moment().format('DD/MM/YY HH:MM:ss.SSS')} | ${time.slice(-6)}`;
      },
      colors: {
        stamp: 'yellow',
        label: 'white',
        metadata: 'green',
      },
      metadata: () => (this.config.metadata ? `  ${(Math.round(process.memoryUsage().rss / 1024 / 1024))}MB`.slice(-6) : null),
    });
  }

  async migrate() {
    try {
      this.init();

      await this.preMigrate();

      this.totalRecords = await this.biCount(this.table);
      this.pages = Math.ceil(this.totalRecords / this.config.pageSize);
      console.log(`${this.totalRecords} records found, page size: ${this.config.pageSize}, total pages: ${this.pages}`);

      let inserted = 0;
      for (let i = 0; i < this.pages; i++) {
        const result = await this.getPage(i);
        console.log('Processing records');
        result.forEach(this.parseRecord.bind(this));
        console.log('Inserting records');
        inserted += (await this.model.bulkCreate(result)).length;
        console.log(`${inserted} records inserted in total`);
      }

      await this.postMigrate();
    } catch (error) {
      console.error(error);
    } finally {
      this.end();
    }
  }

  async preMigrate() {
    console.log('Truncating table');
    await this.model.truncate({ cascade: true });
  }

  parseRecord(record) {
    record.geom = {
      type: 'Point',
      coordinates: [record.longitude, record.latitude],
      crs: { type: 'name', properties: { name: 'EPSG:4326' } },
    };
    record.siasarId = record.siasar_id;
    record.pictureUrl = record.picture_url;
    record.surveyDate = record.survey_date;

    record.adm0 = record.adm_0;
    if (record.adm && record.adm.length) {
      [record.adm1, record.adm2, record.adm3, record.adm4] = record.adm.reverse();
    }

    if (record.variables) {
      Object.keys(record.variables).forEach((variable) => {
        record[this.variables[variable] || variable] = record.variables[variable];
      });
    }

    record.siasarVersion = record.score ? '1' : '2';
  }

  postMigrate() {}

  async getPage(page) {
    console.log(`Getting page ${page + 1}`);
    const result = await this.biQuery(this.query, {
      limit: this.config.pageSize,
      offset: page * this.config.pageSize,
    });
    return result;
  }

  async biQuery(query, replacements, type = this.sequelize.bi.QueryTypes.SELECT) {
    const result = await this.sequelize.bi.query(query, {
      replacements,
      type,
    });
    return result;
  }

  async dataQuery(query, replacements, type = this.sequelize.bi.QueryTypes.SELECT) {
    const result = await this.sequelize.data.query(query, {
      replacements,
      type,
    });
    return result;
  }

  async biCount(table) {
    return (await this.biQuery(`SELECT COUNT(*) FROM ${table};`))[0].count;
  }

  init() {
    console.log(`Migration of ${this.model.name} initialized`);
  }

  end() {
    this.sequelize.bi.close();
    this.sequelize.data.close();
    console.log('Migration finished');
  }

  static calculateScore(value) {
    if (typeof value === 'undefined' || value < 0 || value > 1) return 'N';
    if (value < 0.4) return 'D';
    if (value >= 0.4 && value < 0.7) return 'C';
    if (value >= 0.7 && value < 0.9) return 'B';
    return 'A';
  }
}
