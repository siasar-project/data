import Sequelize from 'sequelize';
import BaseModel from './base_model';

module.exports = class Community extends BaseModel {
  static init(sequelize) {
    return super.init(
      sequelize,
      {
        siasarId: {
          field: 'siasar_id',
          type: Sequelize.INTEGER,
          primaryKey: true,
        },
        name: Sequelize.STRING,
        surveyDate: {
          field: 'survey_date',
          type: Sequelize.DATEONLY,
        },
        country: Sequelize.STRING(2),
        adm0: {
          field: 'adm_0',
          type: Sequelize.STRING,
        },
        adm1: {
          field: 'adm_1',
          type: Sequelize.STRING,
        },
        adm2: {
          field: 'adm_2',
          type: Sequelize.STRING,
        },
        adm3: {
          field: 'adm_3',
          type: Sequelize.STRING,
        },
        adm4: {
          field: 'adm_4',
          type: Sequelize.STRING,
        },
        longitude: Sequelize.DOUBLE,
        latitude: Sequelize.DOUBLE,
        geom: Sequelize.GEOMETRY('POINT', 4326),
        score: Sequelize.STRING(1),
        siasarVersion: {
          field: 'siasar_version',
          type: Sequelize.STRING,
        },
        pictureUrl: {
          field: 'picture_url',
          type: Sequelize.STRING,
        },
        population: Sequelize.INTEGER,
        households: Sequelize.INTEGER,
        variables: Sequelize.JSON,
        createdAt: {
          field: 'created_at',
          type: Sequelize.DATE,
        },
        updatedAt: {
          field: 'updated_at',
          type: Sequelize.DATE,
        },
        location: {
          type: Sequelize.VIRTUAL,
          get() {
            return `${this.getDataValue('adm1')}, ${this.getDataValue('adm2')}, ${this.getDataValue('adm3')}`;
          },
        },
      },
      'communities',
      ['wsp', 'wshl', 'wssi', 'wsl', 'shl', 'wsi', 'sep', 'wslAcc', 'wslCon',
        'wslSea', 'wslQua', 'shlSsl', 'shlPer', 'shlWat', 'ecs', 'ecsEag', 'ecsCag', 'ecsShe',
        'ecsShs', 'fis', 'fps', 'fhp', 'fus', 'ftb', 'fafl'],
    );
  }

  static associate(models) {
    models.Community.belongsToMany(models.System, {
      through: models.SystemCommunity,
      foreignKey: 'community_id',
      otherKey: 'system_id',
      as: 'systems',
    });
  }
};
