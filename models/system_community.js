import Sequelize from 'sequelize';

module.exports = class SystemCommunity extends Sequelize.Model {
  static init(sequelize) {
    return super.init({
      systemId: {
        field: 'system_id',
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      communityId: {
        field: 'community_id',
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      servedHouseholds: {
        field: 'served_households',
        type: Sequelize.INTEGER,
      },
      createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
      },
      updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
      },
    }, { tableName: 'systems_communities', sequelize });
  }
};
