import Sequelize from 'sequelize';
import BaseModel from './base_model';

module.exports = class TechnicaRegionlProvider extends BaseModel {
  static init(sequelize) {
    return super.init(
      sequelize,
      {
        name: Sequelize.STRING,
        country: Sequelize.STRING(2),
        admLevel: {
          field: 'adm_level',
          type: Sequelize.INTEGER,
        },
        score: Sequelize.STRING(1),
        wsp: Sequelize.FLOAT,
        geom: Sequelize.GEOMETRY('MULTIPOLYGON', 4326),
        createdAt: {
          field: 'created_at',
          type: Sequelize.DATE,
        },
        updatedAt: {
          field: 'updated_at',
          type: Sequelize.DATE,
        },
      },
      'regions',
    );
  }
};
