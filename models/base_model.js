import Sequelize from 'sequelize';

module.exports = class BaseModel extends Sequelize.Model {
  static init(sequelize, attributes, tableName, indicators) {
    if (indicators) {
      attributes.indicators = {
        type: Sequelize.VIRTUAL,
        get() {
          return indicators;
        },
      };
      indicators.forEach((indicator) => {
        attributes[indicator] = {
          field: indicator.split(/(?=[A-Z])/).join('_').toLowerCase(),
          type: Sequelize.FLOAT,
        };
        attributes[`${indicator}Score`] = {
          type: Sequelize.VIRTUAL,
          get() {
            return this.calculateScore(this.getDataValue(indicator));
          },
        };
      });
    }

    return super.init(attributes, { tableName, sequelize });
  }

  calculateScore(value) {
    if (value >= 0.9) return 'A';
    if (value >= 0.7 && value < 0.9) return 'B';
    if (value >= 0.4 && value < 0.7) return 'C';
    return 'D';
  }
};
