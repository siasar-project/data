import fs from 'fs';
import path from 'path';
import sequelize from '../lib/sequelize';

const models = Object.assign({}, ...fs.readdirSync(__dirname)
  .filter(file => (file.indexOf('.') !== 0 && file !== 'index.js' && file !== 'base_model.js'))
  .map((file) => {
    const model = require(path.join(__dirname, file)); // eslint-disable-line
    return {
      [model.name]: model.init(sequelize.data),
    };
  }));

Object.keys(models).forEach(model => (
  typeof models[model].associate === 'function' && models[model].associate(models)
));

module.exports = models;
