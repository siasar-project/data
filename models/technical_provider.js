import Sequelize from 'sequelize';
import BaseModel from './base_model';

module.exports = class TechnicalProvider extends BaseModel {
  static init(sequelize) {
    return super.init(
      sequelize,
      {
        siasarId: {
          field: 'siasar_id',
          type: Sequelize.INTEGER,
          primaryKey: true,
        },
        name: Sequelize.STRING,
        surveyDate: {
          field: 'survey_date',
          type: Sequelize.DATEONLY,
        },
        country: Sequelize.STRING(2),
        adm0: {
          field: 'adm_0',
          type: Sequelize.STRING,
        },
        adm1: {
          field: 'adm_1',
          type: Sequelize.STRING,
        },
        adm2: {
          field: 'adm_2',
          type: Sequelize.STRING,
        },
        adm3: {
          field: 'adm_3',
          type: Sequelize.STRING,
        },
        adm4: {
          field: 'adm_4',
          type: Sequelize.STRING,
        },
        longitude: Sequelize.DOUBLE,
        latitude: Sequelize.DOUBLE,
        geom: Sequelize.GEOMETRY('POINT', 4326),
        score: Sequelize.STRING(1),
        siasarVersion: {
          field: 'siasar_version',
          type: Sequelize.STRING,
        },
        pictureUrl: {
          field: 'picture_url',
          type: Sequelize.STRING,
        },
        servedHouseholds: {
          field: 'served_households',
          type: Sequelize.INTEGER,
        },
        providerType: {
          field: 'provider_type',
          type: Sequelize.STRING,
        },
        variables: Sequelize.JSON,
        communities: Sequelize.JSON,
        createdAt: {
          field: 'created_at',
          type: Sequelize.DATE,
        },
        updatedAt: {
          field: 'updated_at',
          type: Sequelize.DATE,
        },
      },
      'technical_providers',
    );
  }
};
