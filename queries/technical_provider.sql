SELECT
  pat_seq as siasar_id,
  pat_a_005_001 as name,
  pat_a_002_001 AS survey_date,
  p.pais_abreviatura as adm_0,
  (
    WITH RECURSIVE adm as (
      SELECT tda_seq, tda_nome, tda_pai
      FROM tb_divisao_admin
      WHERE tda_seq = t.pat_a_007_001
      UNION ALL
      SELECT tda.tda_seq, tda.tda_nome, tda.tda_pai
      FROM tb_divisao_admin tda
      JOIN adm a ON tda.tda_seq = a.tda_pai
    )
    SELECT json_agg(tda_nome) FROM adm
  ) as adm,
  pat_a_010_001 as latitude,
  pat_a_011_001 as longitude,
  t.pat_a_004_001 AS picture_url,
  tt.taxt_nome as provider_type,
  t.pat_b_001_001 as served_households,
  p.pais_sigla as country
FROM tb_pat t
LEFT JOIN tb_pais p ON p.pais_sigla = t.pais_sigla
LEFT JOIN tb_taxo_termo tt ON t.pat_a_006_001 = tt.taxt_seq