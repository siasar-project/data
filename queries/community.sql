SELECT
  t.comun_seq AS siasar_id,
  t.com_a_003_001 AS name,
  t.com_a_001_001 AS survey_date,
  p.pais_abreviatura AS adm_0,
  (
    WITH RECURSIVE adm as (
      SELECT tda_seq, tda_nome, tda_pai
      FROM tb_divisao_admin
      WHERE tda_seq = t.com_a_004_001
      UNION ALL
      SELECT tda.tda_seq, tda.tda_nome, tda.tda_pai
      FROM tb_divisao_admin tda
      JOIN adm a ON tda.tda_seq = a.tda_pai
    )
    SELECT json_agg(tda_nome) FROM adm
  ) as adm,
  t.com_a_014_001 AS population,
  t.com_a_018_001 AS households,
  t.com_a_008_001 AS longitude,
  t.url_imagem AS picture_url,
  t.com_a_007_001 AS latitude,
  t.pais_sigla AS country,
  t.classificacao as score,
  (
    SELECT json_object_agg(lower(v.varia_nome), vdw.vardw_valor)
    FROM tb_variavel_dw vdw
    JOIN tb_variavel v ON v.varia_seq = vdw.varia_seq
    WHERE vdw.vardw_entidade = t.comun_seq
  ) AS variables
FROM tb_comunidade t
LEFT JOIN tb_pais p ON p.pais_sigla = t.pais_sigla
ORDER BY t.comun_seq