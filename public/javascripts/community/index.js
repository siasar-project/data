$(() => {
  const map = L.map('map').setView([0, 0], 2);

  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
    renderer: L.svg(),
  }).addTo(map);

  L.geoJSON(window.geoJSON, {
    crs: L.CRS.EPSG4326,
    pointToLayer(feature, latlng) {
      return new L.CircleMarker(latlng, {
        radius: 5,
        className: `circle-score background-score-${feature.properties.score}`,
      }).bindTooltip(`${feature.properties.siasarId}: ${feature.properties.name}`)
        .on('click', () => {
          window.open(`/communities/${feature.properties.siasarId}`);
        });
    },
  }).addTo(map);

  const scoreChart = new Chart(document.getElementById('scoreChart'), { // eslint-disable-line
    type: 'bar',
    data: {
      labels: window.scoreChart.labels,
      datasets: [{
        label: '# of Communitites',
        data: window.scoreChart.data,
        backgroundColor: ['#54BA46', '#FFFF39', '#FF9326', '#C92429'],
      }],
    },
    options: {
      legend: {
        display: false,
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
          },
        }],
      },
    },
  });

  const countryChart = new Chart(document.getElementById('countryChart'), { // eslint-disable-line
    type: 'bar',
    data: {
      labels: window.countryChart.labels,
      datasets: [{
        label: '# of Communitites',
        data: window.countryChart.data,
        backgroundColor: window.defaultColors,
      }],
    },
    options: {
      legend: {
        display: false,
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
          },
        }],
      },
    },
  });

  const dateChart = new Chart(document.getElementById('dateChart'), { // eslint-disable-line
    type: 'line',
    data: {
      labels: window.dateChart.labels,
      datasets: [{
        label: '# of Communitites',
        data: window.dateChart.data,
      }],
    },
    options: {
      legend: {
        display: false,
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
          },
        }],
        xAxes: [{
          type: 'time',
          distribution: 'series',
        }],
      },
    },
  });
});
