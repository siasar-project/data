$(() => {
  const map = L.map('map').setView([window.record.latitude, window.record.longitude], 13);
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors' }).addTo(map);
  L.marker([window.record.latitude, window.record.longitude]).addTo(map);
});
