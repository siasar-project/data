$(() => {
  const scoreChart = new Chart(document.getElementById('scoreChart'), { // eslint-disable-line
    type: 'bar',
    data: {
      labels: window.scoreChart.labels,
      datasets: [{
        label: '# of System',
        data: window.scoreChart.data,
        backgroundColor: ['#54BA46', '#FFFF39', '#FF9326', '#C92429'],
      }],
    },
    options: {
      legend: {
        display: false,
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
          },
        }],
      },
    },
  });

  const countryChart = new Chart(document.getElementById('countryChart'), { // eslint-disable-line
    type: 'bar',
    data: {
      labels: window.countryChart.labels,
      datasets: [{
        label: '# of System',
        data: window.countryChart.data,
        backgroundColor: window.defaultColors,
      }],
    },
    options: {
      legend: {
        display: false,
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
          },
        }],
      },
    },
  });

  const dateChart = new Chart(document.getElementById('dateChart'), { // eslint-disable-line
    type: 'line',
    data: {
      labels: window.dateChart.labels,
      datasets: [{
        label: '# of System',
        data: window.dateChart.data,
      }],
    },
    options: {
      legend: {
        display: false,
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
          },
        }],
        xAxes: [{
          type: 'time',
          distribution: 'series',
        }],
      },
    },
  });
});
