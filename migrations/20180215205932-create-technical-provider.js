module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('technical_providers', {
    siasar_id: {
      primaryKey: true,
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    name: Sequelize.STRING,
    survey_date: Sequelize.DATEONLY,
    country: Sequelize.STRING(2),
    adm_0: Sequelize.STRING,
    adm_1: Sequelize.STRING,
    adm_2: Sequelize.STRING,
    adm_3: Sequelize.STRING,
    adm_4: Sequelize.STRING,
    longitude: Sequelize.DOUBLE,
    latitude: Sequelize.DOUBLE,
    geom: Sequelize.GEOMETRY('POINT', 4326),
    score: Sequelize.STRING(1),
    siasar_version: Sequelize.STRING,
    picture_url: Sequelize.STRING,
    served_households: Sequelize.INTEGER,
    provider_type: Sequelize.STRING,
    variables: Sequelize.JSON,
    communities: Sequelize.JSON,
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
  })
    .then(() => queryInterface.addIndex('technical_providers', ['name']))
    .then(() => queryInterface.addIndex('technical_providers', ['survey_date']))
    .then(() => queryInterface.addIndex('technical_providers', ['country']))
    .then(() => queryInterface.addIndex('technical_providers', ['adm_0']))
    .then(() => queryInterface.addIndex('technical_providers', ['adm_1']))
    .then(() => queryInterface.addIndex('technical_providers', ['adm_2']))
    .then(() => queryInterface.addIndex('technical_providers', ['adm_3']))
    .then(() => queryInterface.addIndex('technical_providers', ['score']))
    .then(() => queryInterface.addIndex('technical_providers', ['siasar_version']))
    .then(() => queryInterface.addIndex('technical_providers', {
      fields: ['geom'],
      using: 'GIST',
    })),

  down: queryInterface => queryInterface.dropTable('technical_providers'),
};
