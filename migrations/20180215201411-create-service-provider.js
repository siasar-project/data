module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('service_providers', {
    siasar_id: {
      primaryKey: true,
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    name: Sequelize.STRING,
    survey_date: Sequelize.DATEONLY,
    country: Sequelize.STRING(2),
    adm_0: Sequelize.STRING,
    adm_1: Sequelize.STRING,
    adm_2: Sequelize.STRING,
    adm_3: Sequelize.STRING,
    adm_4: Sequelize.STRING,
    longitude: Sequelize.DOUBLE,
    latitude: Sequelize.DOUBLE,
    geom: Sequelize.GEOMETRY('POINT', 4326),
    score: Sequelize.STRING(1),
    siasar_version: Sequelize.STRING,
    picture_url: Sequelize.STRING,
    women_count: Sequelize.INTEGER,
    provider_type: Sequelize.STRING,
    month_billing: Sequelize.DOUBLE,
    legal_status: Sequelize.STRING,
    served_households: Sequelize.INTEGER,
    sep: Sequelize.FLOAT,
    sep_org: Sequelize.FLOAT,
    sep_opm: Sequelize.FLOAT,
    sep_eco: Sequelize.FLOAT,
    sep_env: Sequelize.FLOAT,
    variables: Sequelize.JSON,
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
  })
    .then(() => queryInterface.addIndex('service_providers', ['name']))
    .then(() => queryInterface.addIndex('service_providers', ['survey_date']))
    .then(() => queryInterface.addIndex('service_providers', ['country']))
    .then(() => queryInterface.addIndex('service_providers', ['adm_0']))
    .then(() => queryInterface.addIndex('service_providers', ['adm_1']))
    .then(() => queryInterface.addIndex('service_providers', ['adm_2']))
    .then(() => queryInterface.addIndex('service_providers', ['adm_3']))
    .then(() => queryInterface.addIndex('service_providers', ['score']))
    .then(() => queryInterface.addIndex('service_providers', ['siasar_version']))
    .then(() => queryInterface.addIndex('service_providers', {
      fields: ['geom'],
      using: 'GIST',
    })),

  down: queryInterface => queryInterface.dropTable('service_providers'),
};
