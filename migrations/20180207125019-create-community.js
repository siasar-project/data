module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('communities', {
    siasar_id: {
      primaryKey: true,
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    name: Sequelize.STRING,
    survey_date: Sequelize.DATEONLY,
    country: Sequelize.STRING(2),
    adm_0: Sequelize.STRING,
    adm_1: Sequelize.STRING,
    adm_2: Sequelize.STRING,
    adm_3: Sequelize.STRING,
    adm_4: Sequelize.STRING,
    longitude: Sequelize.DOUBLE,
    latitude: Sequelize.DOUBLE,
    geom: Sequelize.GEOMETRY('POINT', 4326),
    score: Sequelize.STRING(1),
    siasar_version: Sequelize.STRING,
    picture_url: Sequelize.STRING,
    population: Sequelize.INTEGER,
    households: Sequelize.INTEGER,
    wsp: Sequelize.FLOAT,
    wshl: Sequelize.FLOAT,
    wssi: Sequelize.FLOAT,
    wsl: Sequelize.FLOAT,
    shl: Sequelize.FLOAT,
    wsi: Sequelize.FLOAT,
    sep: Sequelize.FLOAT,
    wsl_acc: Sequelize.FLOAT,
    wsl_con: Sequelize.FLOAT,
    wsl_sea: Sequelize.FLOAT,
    wsl_qua: Sequelize.FLOAT,
    shl_ssl: Sequelize.FLOAT,
    shl_per: Sequelize.FLOAT,
    shl_wat: Sequelize.FLOAT,
    shl_com: Sequelize.FLOAT,
    wsi_aut: Sequelize.FLOAT,
    wsi_inf: Sequelize.FLOAT,
    wsi_pro: Sequelize.FLOAT,
    wsi_tre: Sequelize.FLOAT,
    sep_org: Sequelize.FLOAT,
    sep_opm: Sequelize.FLOAT,
    sep_eco: Sequelize.FLOAT,
    sep_env: Sequelize.FLOAT,
    ecs: Sequelize.FLOAT,
    ecs_eag: Sequelize.FLOAT,
    ecs_cag: Sequelize.FLOAT,
    ecs_she: Sequelize.FLOAT,
    ecs_shs: Sequelize.FLOAT,
    fis: Sequelize.FLOAT,
    fps: Sequelize.FLOAT,
    fhp: Sequelize.FLOAT,
    fus: Sequelize.FLOAT,
    ftb: Sequelize.FLOAT,
    fafl: Sequelize.FLOAT,
    variables: Sequelize.JSON,
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
  })
    .then(() => queryInterface.addIndex('communities', ['name']))
    .then(() => queryInterface.addIndex('communities', ['survey_date']))
    .then(() => queryInterface.addIndex('communities', ['country']))
    .then(() => queryInterface.addIndex('communities', ['adm_0']))
    .then(() => queryInterface.addIndex('communities', ['adm_1']))
    .then(() => queryInterface.addIndex('communities', ['adm_2']))
    .then(() => queryInterface.addIndex('communities', ['adm_3']))
    .then(() => queryInterface.addIndex('communities', ['score']))
    .then(() => queryInterface.addIndex('communities', ['siasar_version']))
    .then(() => queryInterface.addIndex('communities', {
      fields: ['geom'],
      using: 'GIST',
    })),

  down: queryInterface => queryInterface.dropTable('communities'),
};
