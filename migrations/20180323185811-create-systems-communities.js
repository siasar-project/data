module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable(
    'systems_communities',
    {
      system_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'systems',
          key: 'siasar_id',
        },
      },
      community_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: {
          model: 'communities',
          key: 'siasar_id',
        },
      },
      served_households: Sequelize.INTEGER,
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      },
    },
  ),

  down: queryInterface => queryInterface.dropTable('systems_communities'),
};
