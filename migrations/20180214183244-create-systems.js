module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('systems', {
    siasar_id: {
      primaryKey: true,
      type: Sequelize.INTEGER,
      allowNull: false,
    },
    name: Sequelize.STRING,
    survey_date: Sequelize.DATEONLY,
    country: Sequelize.STRING(2),
    adm_0: Sequelize.STRING,
    adm_1: Sequelize.STRING,
    adm_2: Sequelize.STRING,
    adm_3: Sequelize.STRING,
    adm_4: Sequelize.STRING,
    longitude: Sequelize.DOUBLE,
    latitude: Sequelize.DOUBLE,
    geom: Sequelize.GEOMETRY('POINT', 4326),
    score: Sequelize.STRING(1),
    siasar_version: Sequelize.STRING,
    picture_url: Sequelize.STRING,
    building_year: Sequelize.INTEGER,
    served_households: Sequelize.INTEGER,
    supply_type: Sequelize.STRING,
    wsi: Sequelize.FLOAT,
    wsi_aut: Sequelize.FLOAT,
    wsi_inf: Sequelize.FLOAT,
    wsi_pro: Sequelize.FLOAT,
    wsi_tre: Sequelize.FLOAT,
    variables: Sequelize.JSON,
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
  })
    .then(() => queryInterface.addIndex('systems', ['name']))
    .then(() => queryInterface.addIndex('systems', ['survey_date']))
    .then(() => queryInterface.addIndex('systems', ['country']))
    .then(() => queryInterface.addIndex('systems', ['adm_0']))
    .then(() => queryInterface.addIndex('systems', ['adm_1']))
    .then(() => queryInterface.addIndex('systems', ['adm_2']))
    .then(() => queryInterface.addIndex('systems', ['adm_3']))
    .then(() => queryInterface.addIndex('systems', ['score']))
    .then(() => queryInterface.addIndex('systems', ['siasar_version']))
    .then(() => queryInterface.addIndex('systems', {
      fields: ['geom'],
      using: 'GIST',
    })),

  down: queryInterface => queryInterface.dropTable('systems'),
};
