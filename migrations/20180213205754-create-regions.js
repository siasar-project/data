module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('regions', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    name: Sequelize.STRING,
    country: Sequelize.STRING(2),
    adm_level: Sequelize.INTEGER,
    score: Sequelize.STRING(1),
    geom: Sequelize.GEOMETRY('MULTIPOLYGON', 4326),
    wsp: Sequelize.FLOAT,
    created_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
    },
  })
    .then(() => queryInterface.addIndex('regions', ['adm_level']))
    .then(() => queryInterface.addIndex('regions', ['country']))
    .then(() => queryInterface.addIndex('regions', ['name']))
    .then(() => queryInterface.addIndex('regions', ['score']))
    .then(() => queryInterface.addIndex('regions', ['wsp']))
    .then(() => queryInterface.addIndex('regions', {
      fields: ['geom'],
      using: 'GIST',
    })),

  down: queryInterface => queryInterface.dropTable('regions'),
};
